class UsersController < ApplicationController
  skip_before_filter :verify_authenticity_token  

	def index
		@users = Article.all
		respond_to do |format|
			format.json { render json: @users }
			format.xml { render xml: @users }
		end
	end

	def create
		respond_to do |format|
			@user = User.new
			@user.username = params[:username]

			if @user.save
					format.json { render json: @user }
					format.xml { render xml: @user }
			else
				format.json { render json: @user.errors, status: :unprocessable_entity }
	  		format.xml { render xml: @user.errors, status: :unprocessable_entity }
	  	end
	  end
	end
end
