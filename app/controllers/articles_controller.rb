class ArticlesController < ApplicationController
  skip_before_filter :verify_authenticity_token  

	def index
		@articles = Article.all
		respond_to do |format|
			format.json { render json: @articles }
			format.xml { render xml: @articles }
		end
	end

	def create
		respond_to do |format|
			@article = Article.new
			@article.title = params[:title]
			@article.body = params[:body]
			@article.user_id = params[:user_id]

			if @article.save
					format.json { render json: @article }
					format.xml { render xml: @article }
			else
				format.json { render json: @article.errors, status: :unprocessable_entity }
	  		format.xml { render xml: @article.errors, status: :unprocessable_entity }
	  	end
	  end
	end
end
