class Article < ActiveRecord::Base
	belongs_to :user, :class_name => 'User', :foreign_key => 'user_id'
	validates :title, :body, :user, presence: true
end
